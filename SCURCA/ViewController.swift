//
//  ViewController.swift
//  SCURCA
//
//  Created by Kevin Sampson on 5/25/21.
//

import UIKit
import AVKit
import AVFoundation

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    private func playVideo1() {
            guard let path = Bundle.main.path(forResource: "video1", ofType:"mp4") else {
                debugPrint("video1.mp4 not found")
                return
            }
            let player = AVPlayer(url: URL(fileURLWithPath: path))
            let playerController = AVPlayerViewController()
            playerController.player = player
            present(playerController, animated: true) {
                player.play()
            }
        }
    
   
    
    
    @IBAction func buttonAction1(_ sender: Any, forEvent event: UIEvent) {
        playVideo1()
    }
    
    
    
    

}

